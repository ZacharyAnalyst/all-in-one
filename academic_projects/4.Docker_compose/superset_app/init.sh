#!/bin/sh

pip install clickhouse-sqlalchemy
superset fab create-admin --username "$ADMIN_USERNAME" --firstname "$FIRSTNAME" --lastname "$LASTNAME" --email "$EMAIL" --password "$PASSWORD"
superset db upgrade
superset init
superset run -p 8088 -h 0.0.0.0
