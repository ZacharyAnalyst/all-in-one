# Пет проекты:

### [*academic projects*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects)

- [*1.A/А test*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/1.AA_test) \
Применил: Метод Колмогорова-Смирнова и T-test Стьюдента
Инструменты: Python, Git

- [*2.A/B test*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/2.AB_test) \
Применял: Bootstrap метод сравнения средних и медиан, а также строил Bootsrap ДИ \
Инструменты: Python, Git

- [*3.Поиск паттернов*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/3.Purch_patterns) 

- [*4.Поднятие стека приложений в Docker, включающий БД Clickhouse, PostgreSQL*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/4.Docker_compose/superset_app)

- [*5.Aвтоматическая отправка отчетности с использованием Airflow и VK API*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/5.Airflow)

- [*6.Аналитические кейсы SQL с Habr*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/6.SQL)

- [*7.Визуализаци в Tableau*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/academic_projects/7.Tableau)

### [*test asignments*</a>:](https://gitlab.com/ZacharyAnalyst/all-in-one/-/tree/main/test_assignments)

- Aviasales Test assignment

